# Smoke use cases

## Goal

The goal of this docker is to run End to End use cases on ONAP in order to
check the solution. The testcases defined in this docker MUST be PASS to
validate the release.
The test cases can be run using Robot framework or onap-test (ONAP python SDK).
Bash, python and unit test drivers also exist. Additionnal drivers can be added
but the Dockerfile must be adapted accordingly.

The tests are:

* nginx_cnf: it onboard/distribute/deploy a single nginx helm package in ONAP using
  VNF-API. The components used are SDC, SO, AA&I, SDNC and multicloud.

## Usage

### Configuration

### Command

### Output
