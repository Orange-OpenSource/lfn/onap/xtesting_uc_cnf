#!/usr/bin/env python
"""Basic CNF test case."""
import logging
import time

import onap_tests.scenario.e2e as e2e
from xtesting.core import testcase


class Nginx(testcase.TestCase):
    """Onboard then instantiate a nginx helm package though ONAP."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init Nginx."""
        if "case_name" not in kwargs:
            kwargs["case_name"] = 'nginx_cnf'
        super(Nginx, self).__init__(**kwargs)
        self.__logger.debug("Nginx cnf init started")
        self.test = e2e.E2E(service_name='nginxk8s01')
        self.start_time = None
        self.stop_time = None
        self.result = 0

    def run(self):
        """Run onap_tests with ngninx helm package."""
        self.start_time = time.time()
        self.__logger.debug("start time")
        self.test.execute()
        self.__logger.info("CNF nginx successfully created")
        self.test.clean()
        # Clean is part of the test
        self.__logger.info("CNF cleaned")
        self.result = 100
        self.stop_time = time.time()
        return testcase.TestCase.EX_OK

    def clean(self):
        """Clean CNF."""
        pass
